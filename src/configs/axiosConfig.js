import axios from 'axios'
const axiosConfig = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
})

axiosConfig.defaults.headers.post['Content-Type'] = 'application/json'
axiosConfig.interceptors.request.use(
  (config) => {
    // ** Get token from localStorage
    const accessToken = localStorage.getItem('accessToken')

    // ** If token is present add it to request's Authorization Header
    if (accessToken) {
      // ** eslint-disable-next-line no-param-reassign
      config.headers.Authorization = `Bearer ${accessToken}`
    }
    return config
  },
  (error) => Promise.reject(error),
)

export default axiosConfig
