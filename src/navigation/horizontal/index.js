import { Home, Clipboard, Gift } from 'react-feather'

export default [
  {
    id: 'home',
    title: 'Home',
    icon: <Home size={20} />,
    navLink: '/home',
  },
  {
    id: 'logsPage',
    title: 'Logs',
    icon: <Clipboard size={20} />,
    navLink: '/logs',
  },
]
