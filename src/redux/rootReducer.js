// ** Reducers Imports
import layout from './layout'
import navbar from './navbar'
import search from './search'

const rootReducer = { navbar, layout, search }

export default rootReducer
