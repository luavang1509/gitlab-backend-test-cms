// ** Redux Imports
import { createSlice } from '@reduxjs/toolkit'

export const searchSlice = createSlice({
  name: 'search',
  initialState: {
    filter: {
      ownerMobile: '',
      voucherCode: '',
      brandCode: '',
      rewardName: '',
      offerId: '',
      submitStartDate: '',
      submitEndDate: '',
      deliveryStartDate: '',
      deliveryEndDate: '',
      status: '',
    },
  },
  reducers: {
    handleSubmitSearchFields: (state, action) => {
      state.filter = action.payload
    },
  },
})

export const { handleSubmitSearchFields } = searchSlice.actions

export default searchSlice.reducer
