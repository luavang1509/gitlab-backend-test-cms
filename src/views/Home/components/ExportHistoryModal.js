import React, { useState, useEffect } from 'react'
import { notification } from 'antd'
import DataTable from 'react-data-table-component'
import { ChevronDown, Clipboard } from 'react-feather'
import { Button, Col, Container, Modal, ModalBody, ModalHeader, Row, Spinner } from 'reactstrap'
import axiosConfig from '../../../configs/axiosConfig'
import { columns } from '../historyExportData'
import ReactPaginate from 'react-paginate'

const ExportHistoryModal = ({ showExportHistoryModal, setShowExportHistoryModal }) => {
  const hideModal = () => {
    setShowExportHistoryModal(false)
  }
  const [pagination, setPagination] = useState({
    currentPage: 1,
    pageSize: 10,
    totalPages: 1,
    totalRows: 0,
  })
  const [history, setHistory] = useState([])
  const [loading, setLoading] = useState(false)

  const getExportHistory = async (page) => {
    setLoading(true)
    try {
      await axiosConfig.get('/export-history', { params: { page } }).then((res) => {
        setHistory(res.data.data)
        setPagination(res.data.meta)
        setLoading(false)
      })
    } catch (error) {
      setLoading(false)
      notification.error({
        message: 'Failed to get history',
        duration: 2,
      })
    }
  }

  useEffect(() => {
    getExportHistory()
  }, [])

  // ** Function to handle filter
  const handlePagination = (page) => {
    getExportHistory(page.selected + 1)
  }

  // ** Custom Pagination
  const CustomPagination = () => {
    return (
      <ReactPaginate
        previousLabel={''}
        nextLabel={''}
        forcePage={pagination?.currentPage - 1}
        onPageChange={(page) => handlePagination(page)}
        pageCount={pagination?.totalPages}
        breakLabel={'...'}
        pageRangeDisplayed={2}
        marginPagesDisplayed={2}
        activeClassName="active"
        pageClassName="page-item"
        breakClassName="page-item"
        nextLinkClassName="page-link"
        pageLinkClassName="page-link"
        breakLinkClassName="page-link"
        previousLinkClassName="page-link"
        nextClassName="page-item next-item"
        previousClassName="page-item prev-item"
        containerClassName={
          'pagination react-paginate separated-pagination pagination-sm justify-content-end pe-1 my-1'
        }
      />
    )
  }

  return (
    <Modal
      isOpen={showExportHistoryModal}
      onClosed={hideModal}
      toggle={() => setShowExportHistoryModal(!showExportHistoryModal)}
      className="modal-dialog-centered modal-lg"
    >
      <ModalHeader toggle={() => setShowExportHistoryModal(!showExportHistoryModal)}>
        <Clipboard />
        <span>History Exports</span>
      </ModalHeader>

      <ModalBody>
        <Row className="p-1 react-dataTable">
          {!loading ? (
            <DataTable
              dense
              noHeader
              pagination
              data={history}
              columns={columns}
              className="react-dataTable"
              sortIcon={<ChevronDown size={10} />}
              paginationComponent={CustomPagination}
              paginationDefaultPage={pagination?.currentPage}
            />
          ) : (
            <div className="f-center flex-column gap-1 p-4">
              <Spinner style={{ color: '#f9d84b', width: '40px', height: '40px' }} />
              Chờ xíu nha...
            </div>
          )}
        </Row>
        <Row>
          <Col className="d-flex justify-content-end gap-1">
            <Button color="primary" onClick={hideModal}>
              Đóng
            </Button>
          </Col>
        </Row>
      </ModalBody>
    </Modal>
  )
}

export default ExportHistoryModal
