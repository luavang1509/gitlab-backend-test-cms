import React from 'react'
import { AlertCircle, Download } from 'react-feather'
import { useDispatch, useSelector } from 'react-redux'
import { Badge, Button, Col, Modal, ModalBody, ModalHeader, Row } from 'reactstrap'
import axiosConfig from '../../../configs/axiosConfig'
import { handleSubmitSearchFields } from '../../../redux/search'

const ExportModal = ({ showExportModal, setShowExportModal, search }) => {
  //redux
  const filterStore = useSelector((state) => state.search.filter)
  const dispatch = useDispatch()

  const hideModal = () => {
    setShowExportModal(false)
  }

  // ** Downloads CSV
  function downloadCSV(csv) {
    const universalBOM = '\uFEFF'
    const link = document.createElement('a')

    if (csv === null) return

    const filename = 'export.csv'

    link.setAttribute('href', `data:text/csv; charset=utf-8, ${encodeURIComponent(universalBOM + csv)}`)
    link.setAttribute('download', filename)
    link.click()
  }

  const handleExport = async (mark) => {
    let queryParams = {}
    const isEmpty = (value) => {
      return value === null || value === '' || value === undefined
    }

    for (const [key, value] of Object.entries(filterStore)) {
      if (!isEmpty(value)) queryParams = { ...queryParams, [key]: value }
    }

    await axiosConfig
      .get('/order/csv', {
        params: {
          ...queryParams,
          delivered: mark,
        },
      })
      .then((response) => {
        downloadCSV(response.data)
        dispatch(handleSubmitSearchFields(search))
        hideModal()
      })
  }
  return (
    <Modal
      isOpen={showExportModal}
      onClosed={hideModal}
      toggle={() => setShowExportModal(!showExportModal)}
      className="modal-dialog-centered modal-lg"
    >
      <ModalHeader toggle={() => setShowExportModal(!showExportModal)}>
        <Download />
        <span>Export to CSV</span>
      </ModalHeader>
      <ModalBody>
        <Row>
          <p>Bạn có muốn đánh dấu gửi đơn cho những dòng này?</p>
          {JSON.stringify(search) !== JSON.stringify(filterStore) && (
            <p>
              <div className="exportModal-warn">
                <AlertCircle />
                <span>Có thay đổi trên thanh search nhưng bạn chưa chọn tìm</span>
              </div>
            </p>
          )}
        </Row>
        <Row>
          <Col className="d-flex justify-content-end gap-1">
            <Button onClick={() => handleExport(true)} color="primary">
              Đánh dấu
            </Button>
            <Button onClick={() => handleExport(false)} outline>
              Không
            </Button>
          </Col>
        </Row>
      </ModalBody>
    </Modal>
  )
}

export default ExportModal
