import { React, useContext } from 'react'
import { ShepherdTour, ShepherdTourContext } from 'react-shepherd'
import { Button } from 'reactstrap'

// ** Styles
import 'shepherd.js/dist/css/shepherd.css'
import '@styles/react/libs/shepherd-tour/shepherd-tour.scss'

const backBtnClass = 'btn btn-sm btn-outline-primary',
  nextBtnClass = 'btn btn-sm btn-primary btn-next'

let instance = null

const steps = [
  {
    id: 'search',
    title: 'Thanh search',
    text: 'Nhấn ENTER để tìm kiếm',
    attachTo: { element: '.searchPane', on: 'bottom' },
    cancelIcon: {
      enabled: true,
    },
    buttons: [
      {
        action: () => instance.cancel(),
        classes: backBtnClass,
        text: 'Bỏ qua',
      },
      {
        text: 'Tiếp theo',
        classes: nextBtnClass,
        action: () => instance.next(),
      },
    ],
  },
  {
    id: 'search',
    title: 'Nút tìm kiếm',
    text: 'Đây là nút tìm kiếm',
    attachTo: { element: '#search-btn', on: 'bottom' },
    cancelIcon: {
      enabled: true,
    },
    buttons: [
      {
        text: 'Bỏ qua',
        classes: backBtnClass,
        action: () => instance.cancel(),
      },
      {
        text: 'Quay lại',
        classes: backBtnClass,
        action: () => instance.back(),
      },
      {
        text: 'Tiếp theo',
        classes: nextBtnClass,
        action: () => instance.next(),
      },
    ],
  },
  {
    id: 'export',
    title: 'Nút xuất',
    text: 'Xuất sang file .csv',
    attachTo: { element: '#export-btn', on: 'bottom' },
    cancelIcon: {
      enabled: true,
    },
    buttons: [
      {
        text: 'Bỏ qua',
        classes: backBtnClass,
        action: () => instance.cancel(),
      },
      {
        text: 'Quay lại',
        classes: backBtnClass,
        action: () => instance.back(),
      },
      {
        text: 'Tiếp theo',
        classes: nextBtnClass,
        action: () => instance.next(),
      },
    ],
  },
  {
    id: 'history',
    title: 'Lịch sử',
    text: 'Xem lịch sử các file đã export',
    attachTo: { element: '#history-btn', on: 'bottom' },
    cancelIcon: {
      enabled: true,
    },
    buttons: [
      {
        text: 'Bỏ qua',
        classes: backBtnClass,
        action: () => instance.cancel(),
      },
      {
        text: 'Quay lại',
        classes: backBtnClass,
        action: () => instance.back(),
      },
      {
        text: 'Tiếp theo',
        classes: nextBtnClass,
        action: () => instance.next(),
      },
    ],
  },
  {
    id: 'row',
    title: 'Bảng các đơn hàng',
    text: 'Row có thể nhấn vào để hiển thị thông tin nhận hàng',
    attachTo: { element: '.react-dataTable', on: 'top' },
    cancelIcon: {
      enabled: true,
    },
    buttons: [
      {
        text: 'Bỏ qua',
        classes: backBtnClass,
        action: () => instance.cancel(),
      },
      {
        text: 'Quay lại',
        classes: backBtnClass,
        action: () => instance.back(),
      },
      {
        text: 'Tiếp theo',
        classes: nextBtnClass,
        action: () => instance.next(),
      },
    ],
  },
  {
    id: 'note',
    title: 'Ghi chú',
    text: 'Những đơn hàng nào của người dùng có ghi chú sẽ có cọng lông này',
    attachTo: { element: '.item_note', on: 'right' },
    cancelIcon: {
      enabled: true,
    },
    buttons: [
      {
        text: 'Bỏ qua',
        classes: backBtnClass,
        action: () => instance.cancel(),
      },
      {
        text: 'Quay lại',
        classes: backBtnClass,
        action: () => instance.back(),
      },
      {
        text: 'Kết thúc',
        classes: nextBtnClass,
        action: () => instance.next(),
      },
    ],
  },
]

const Content = () => {
  const tour = useContext(ShepherdTourContext)
  instance = tour

  return (
    <Button color="secondary" onClick={() => tour.start()}>
      Hướng dẫn sử dụng
    </Button>
  )
}

const GuideTour = () => {
  return (
    <ShepherdTour
      steps={steps}
      tourOptions={{
        useModalOverlay: true,
      }}
    >
      <Content />
    </ShepherdTour>
  )
}

export default GuideTour
