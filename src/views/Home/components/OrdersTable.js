// ** React Imports
import { useState, useEffect } from 'react'

// ** Table columns & Expandable Data
import ExpandableTable, { columns } from '../data'

// ** Third Party Components
import ReactPaginate from 'react-paginate'
import { ChevronDown } from 'react-feather'
import DataTable from 'react-data-table-component'
import axiosConfig from '../../../configs/axiosConfig'
import { useSelector } from 'react-redux'

// ** Reactstrap Imports
import { Card, Container, Spinner } from 'reactstrap'
import { useNavigate } from 'react-router-dom'

const OrdersTable = () => {
  // ** State
  const [pagination, setPagination] = useState({
    currentPage: 1,
    pageSize: 10,
    totalPages: 1,
    totalRows: 0,
  })
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(false)
  const [apiCalled, setApiCalled] = useState(false)

  // ** Redux
  const filterStore = useSelector((state) => state.search.filter)

  const navigate = useNavigate()

  const updateTable = async (page) => {
    setLoading(true)
    let queryParams = {}
    const isEmpty = (value) => {
      return value === null || value === '' || value === undefined
    }

    for (const [key, value] of Object.entries(filterStore)) {
      if (!isEmpty(value)) queryParams = { ...queryParams, [key]: value }
    }

    if (page) queryParams = { ...queryParams, page }
    await axiosConfig
      .get('/order', { params: queryParams })
      .then((response) => {
        setData(response.data.data)
        setPagination(response.data.meta)
        setLoading(false)
        setApiCalled(true)
      })
      .catch((error) => {
        setLoading(false)
        if (error?.response?.status === 403)
          navigate('/login', {
            state: {
              message: 'Please login and try again',
            },
          })
      })
  }
  useEffect(() => {
    updateTable()
  }, [filterStore])

  // ** Function to handle filter
  const handlePagination = (page) => {
    updateTable(page.selected + 1)
  }

  // ** Custom Pagination
  const CustomPagination = () => {
    return (
      <ReactPaginate
        previousLabel={''}
        nextLabel={''}
        forcePage={pagination?.currentPage - 1}
        onPageChange={(page) => handlePagination(page)}
        pageCount={pagination?.totalPages}
        breakLabel={'...'}
        pageRangeDisplayed={2}
        marginPagesDisplayed={2}
        activeClassName="active"
        pageClassName="page-item"
        breakClassName="page-item"
        nextLinkClassName="page-link"
        pageLinkClassName="page-link"
        breakLinkClassName="page-link"
        previousLinkClassName="page-link"
        nextClassName="page-item next-item"
        previousClassName="page-item prev-item"
        containerClassName={
          'pagination react-paginate separated-pagination pagination-sm justify-content-end pe-1 my-1'
        }
      />
    )
  }

  return (
    <>
      {loading && !apiCalled && (
        <Container className="f-center flex-column gap-1 p-4">
          <Spinner style={{ color: '#f9d84b', width: '40px', height: '40px' }} />
          Chờ xíu nha...
        </Container>
      )}

      {!loading && apiCalled && (
        <Card className={`react-dataTable ${pagination.currentPage === 1 && 'animate__animated animate__fadeInUp'}`}>
          <DataTable
            noHeader
            pagination
            data={data}
            expandableRows
            columns={columns}
            expandOnRowClicked
            className="react-dataTable"
            sortIcon={<ChevronDown size={10} />}
            paginationComponent={CustomPagination}
            paginationDefaultPage={pagination?.currentPage}
            expandableRowsComponent={ExpandableTable}
          />
        </Card>
      )}
    </>
  )
}

export default OrdersTable
