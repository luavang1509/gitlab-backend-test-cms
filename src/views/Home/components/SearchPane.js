import { React, useState } from 'react'
import Select from 'react-select'
import { Button, Col, Input, Row } from 'reactstrap'

import Flatpickr from 'react-flatpickr'
import { useDispatch, useSelector } from 'react-redux'

import '@styles/react/libs/flatpickr/flatpickr.scss'
import '@styles/react/libs/react-select/_react-select.scss'
import { Clipboard, Download, Search } from 'react-feather'
import { handleSubmitSearchFields } from '../../../redux/search'
import ExportHistoryModal from './ExportHistoryModal'
import ExportModal from './ExportModal'

const SearchPane = () => {
  const [SubmitDatePicker, setSubmitDatePicker] = useState('')
  const [DeliveryDatePicker, setDeliveryDatePicker] = useState('')
  const [showExportModal, setShowExportModal] = useState(false)
  const [showExportHistoryModal, setShowExportHistoryModal] = useState(false)

  //redux
  const dispatch = useDispatch()
  const searchStore = useSelector((state) => state.search)

  const [search, setSearch] = useState(searchStore.filter)

  // ** Function to handle date filter
  const handleDateFilter = (range, dateType) => {
    if (range.length) {
      if (dateType === 'submitDate') {
        setSubmitDatePicker(range)
        setSearch({ ...search, submitStartDate: range[0], submitEndDate: range[1] })
      }
      if (dateType === 'deliveryDate') {
        setDeliveryDatePicker(range)
        setSearch({ ...search, deliveryStartDate: range[0], deliveryEndDate: range[1] })
      }
    } else {
      setSearch({
        ...search,
        submitStartDate: '',
        submitEndDate: '',
        deliveryStartDate: '',
        deliveryEndDate: '',
      })
    }
  }
  const handleSearch = () => {
    dispatch(handleSubmitSearchFields(search))
  }

  const handleKeypress = (e) => {
    //it triggers by pressing the enter key
    if (e.keyCode === 13) {
      handleSearch()
    }
  }

  return (
    <div className="searchPane py-1 my-1">
      <Row>
        <Col onKeyDown={handleKeypress}>
          <Input
            id="ownerMobile"
            placeholder="Owner mobile"
            onChange={(e) => setSearch({ ...search, ownerMobile: e.target.value })}
          />
        </Col>
        <Col onKeyDown={handleKeypress}>
          <Input
            id="voucherCode"
            placeholder="Voucher code"
            onChange={(e) => setSearch({ ...search, voucherCode: e.target.value })}
          />
        </Col>
        <Col onKeyDown={handleKeypress}>
          <Input
            id="brandCode"
            placeholder="Brand code"
            onChange={(e) => setSearch({ ...search, brandCode: e.target.value })}
          />
        </Col>
        <Col onKeyDown={handleKeypress}>
          <Input
            id="rewardName"
            placeholder="Reward name"
            onChange={(e) => setSearch({ ...search, rewardName: e.target.value })}
          />
        </Col>
        <Col onKeyDown={handleKeypress}>
          <Input
            id="offerID"
            placeholder="OfferID"
            onChange={(e) => setSearch({ ...search, offerId: e.target.value })}
          />
        </Col>
        <Col onKeyDown={handleKeypress}>
          <Select
            isClearable
            placeholder="Status"
            classNamePrefix="select"
            className="react-select"
            options={[
              { value: 'New', label: 'New' },
              { value: 'Delivered', label: 'Delivered' },
            ]}
            onChange={(selected) => setSearch({ ...search, status: selected?.value })}
          />
        </Col>
        <Col onKeyDown={handleKeypress}>
          <Flatpickr
            id="submitDate"
            className="form-control"
            placeholder="Submit Date"
            value={SubmitDatePicker}
            options={{ mode: 'range', dateFormat: 'd/m/Y' }}
            onChange={(date) => handleDateFilter(date, 'submitDate')}
          />
        </Col>
        <Col onKeyDown={handleKeypress}>
          <Flatpickr
            id="deliveryDate"
            className="form-control"
            placeholder="Delivery Date"
            value={DeliveryDatePicker}
            options={{ mode: 'range', dateFormat: 'd/m/Y' }}
            onChange={(date) => handleDateFilter(date, 'deliveryDate')}
          />
        </Col>
        <Col className="action-btns d-flex gap-1">
          <Button id="search-btn" className="icon-btn" color="primary" onClick={handleSearch}>
            <Search />
          </Button>
          <Button id="export-btn" onClick={() => setShowExportModal(true)} className="icon-btn" color="primary">
            <Download />
          </Button>
          <Button id="history-btn" onClick={() => setShowExportHistoryModal(true)} className="icon-btn" color="primary">
            <Clipboard />
          </Button>
        </Col>
      </Row>

      {/* Modals */}
      <ExportModal showExportModal={showExportModal} setShowExportModal={setShowExportModal} search={search} />
      {showExportHistoryModal && (
        <ExportHistoryModal
          showExportHistoryModal={showExportHistoryModal}
          setShowExportHistoryModal={setShowExportHistoryModal}
        />
      )}
    </div>
  )
}

export default SearchPane
