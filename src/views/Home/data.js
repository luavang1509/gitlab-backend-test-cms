// ** Third Party Components
import moment from 'moment/moment'
import {
  FileText,
  User,
  Home,
  Phone,
  Calendar,
  Gift,
  CreditCard,
  Slack,
  Smartphone,
  Truck,
  Feather,
} from 'react-feather'

// ** Reactstrap Imports
import { Badge } from 'reactstrap'

export const status = {
  New: { title: 'New', color: 'light-success' },
  Delivered: { title: 'Delivered', color: 'light-warning' },
}

// ** Expandable table component
const ExpandableTable = ({ data }) => {
  return (
    <div className="expandable-content p-2">
      <p className="expand-item">
        <User />
        <p className="expand-item_key">Receiver:</p>
        <p className="expand-item_value">{data.fullname}</p>
      </p>
      <p className="expand-item">
        <Home />
        <p className="expand-item_key">Address:</p>
        <p className="expand-item_value">
          {data.address}, {data.district}, {data.province}
        </p>
      </p>
      <p className="expand-item">
        <Phone />
        <p className="expand-item_key">Phone:</p>
        <p className="expand-item_value">{data.mobile}</p>
      </p>
      {data.note && (
        <p className="expand-item">
          <Feather />
          <p className="expand-item_key">Ghi chú:</p>
          <p className="expand-item_value">{data.note}</p>
        </p>
      )}
    </div>
  )
}

// ** Table Headers
const SubmitDate = () => (
  <div className="f-center">
    <Calendar />
    <span>Submit date</span>
  </div>
)
const Voucher = () => (
  <div className="f-center">
    <FileText />
    <span>Voucher Code</span>
  </div>
)
const Reward = () => (
  <div className="f-center">
    <Gift />
    <span>Reward</span>
  </div>
)
const OfferID = () => (
  <div className="f-center">
    <CreditCard />
    <span>OfferID</span>
  </div>
)
const BrandCode = () => (
  <div className="f-center">
    <Slack />
    <span>Brand Code</span>
  </div>
)
const OwnerPhone = () => (
  <div className="f-center">
    <Smartphone />
    <span>Owner Mobile</span>
  </div>
)
const Status = () => (
  <div className="f-center">
    <Truck />
    <span>Status</span>
  </div>
)
const DeliveryDate = () => (
  <div className="f-center">
    <Calendar />
    <span>Delivery date</span>
  </div>
)

// ** Table Common Column
export const columns = [
  {
    name: <SubmitDate />,
    minWidth: '160px',
    cell: (row) => {
      return (
        <div className="f-center gap-1">
          <span>{moment(row.submitDate).format('DD/MM/YYYY')}</span>
          {row.note && (
            <Badge className="item_note" color="light-secondary" pill>
              <Feather />
            </Badge>
          )}
        </div>
      )
    },
  },
  {
    name: <Voucher />,
    minWidth: '180px',
    selector: (row) => row.voucherCode,
  },
  {
    name: <Reward />,
    minWidth: '230px',
    selector: (row) => row.rewardName,
  },
  {
    name: <OfferID />,
    minWidth: '220px',
    cell: (row) => {
      return <div className="table-id text-nowrap">{row.offerId}</div>
    },
  },
  {
    name: <BrandCode />,
    minWidth: '160px',
    selector: (row) => row.brandCode,
  },
  {
    name: <OwnerPhone />,
    minWidth: '160px',
    cell: (row) => {
      return <div className="text-nowrap">{row.ownerMobile}</div>
    },
  },
  {
    name: <Status />,
    minWidth: '110px',
    cell: (row) => {
      return (
        <Badge color={status[row.status].color} pill>
          {status[row.status].title}
        </Badge>
      )
    },
  },
  {
    name: <DeliveryDate />,
    minWidth: '190px',
    sortable: true,
    sortFunction: (a, b) => {
      console.log(a.deliveryDate, b.deliveryDate)
      return new Date(a.deliveryDate) > new Date(b.deliveryDate)
    },
    cell: (row) => {
      return <div>{row.deliveryDate !== '' && moment(row.deliveryDate).format('DD/MM/YYYY')}</div>
    },
  },
]

export default ExpandableTable
