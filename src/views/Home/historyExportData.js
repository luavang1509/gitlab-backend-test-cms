// ** Third Party Components
import moment from 'moment/moment'
import { Calendar, ArrowDownCircle, File, Download } from 'react-feather'

// ** Table Headers
const DeliveryDate = () => (
  <div className="f-center">
    <Calendar />
    <span>Delivery date</span>
  </div>
)
const DownloadedRows = () => (
  <div className="f-center">
    <ArrowDownCircle />
    <span>Downloaded Rows</span>
  </div>
)
const FileCSV = () => (
  <div className="f-center">
    <File />
    <span>File</span>
  </div>
)

// ** Table Common Column
export const columns = [
  {
    name: <DeliveryDate />,
    minWidth: '160px',
    cell: (row) => {
      return <div>{moment(row.deliveryDate).format('DD/MM/YYYY · hh:mm A')}</div>
    },
  },
  {
    name: <DownloadedRows />,
    minWidth: '160px',
    cell: (row) => {
      return <div>{row.totalRows}</div>
    },
  },
  {
    name: <FileCSV />,
    minWidth: '160px',
    cell: (row) => {
      return (
        <div>
          <a href={row.url}>
            <Download />
            &nbsp; Tải về
          </a>
        </div>
      )
    },
  },
]
