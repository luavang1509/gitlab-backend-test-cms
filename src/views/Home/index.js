import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { handleSubmitSearchFields } from '../../redux/search'
import OrdersTable from './components/OrdersTable'
import SearchPane from './components/SearchPane'

// ** Styles
import '@styles/react/libs/tables/react-dataTable-component.scss'
import GuideTour from './components/GuideTour'

const Home = () => {
  // ** Redux
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(
      handleSubmitSearchFields({
        ownerMobile: '',
        voucherCode: '',
        brandCode: '',
        rewardName: '',
        offerId: '',
        submitStartDate: '',
        submitEndDate: '',
        deliveryStartDate: '',
        deliveryEndDate: '',
        status: '',
      }),
    )
  }, [])

  return (
    <>
      <GuideTour />
      <SearchPane />
      <OrdersTable />
    </>
  )
}

export default Home
