import InputPasswordToggle from '@components/input-password-toggle'
import TAPTAPLogo from '@src/assets/images/logo/logo.png'
import { useEffect, useState } from 'react'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { Button, CardTitle, Col, Form, Input, Label, Row } from 'reactstrap'
import '@styles/react/pages/page-authentication.scss'
import useJwt from '../@core/auth/jwt/useJwt'
import { notification } from 'antd'

const Login = () => {
  const illustration = 'login-v2.jpg',
    source = require(`@src/assets/images/pages/${illustration}`).default

  const { jwt } = useJwt({ loginEndpoint: '/auth/sign-in' })
  const [form, setForm] = useState({
    username: '',
    password: '',
  })

  const navigate = useNavigate()
  const location = useLocation()
  const message = location?.state?.message

  useEffect(() => {
    if (message) {
      notification.error({
        message: `Error`,
        description: `${message}`,
        placement: 'topRight',
      })
    }
  }, [])

  const loginHandler = async (e) => {
    e.preventDefault()
    const { username, password } = form
    await jwt
      .login({
        username,
        password,
      })
      .then((response) => {
        if (response?.data?.data?.accessToken) {
          jwt.setToken(response.data.data.accessToken)
          localStorage.setItem('username', username)
          navigate('/')
        }
      })
      .catch((err) => {
        if (err) {
          notification.error({
            message: err?.response?.data?.message,
            duration: 2,
          })
        }
      })
  }

  return (
    <div className="auth-wrapper auth-cover">
      <Row className="auth-inner m-0">
        <Link className="brand-logo" to="/" onClick={(e) => e.preventDefault()}>
          <img src={TAPTAPLogo} alt="" />
        </Link>
        <Col className="d-none d-lg-flex align-items-center p-0" lg="8" sm="12">
          <img style={{ height: '100%', objectFit: 'cover' }} className="img-fluid" src={source} alt="Login Cover" />
        </Col>
        <Col className="d-flex align-items-center auth-bg px-2 p-lg-5" lg="4" sm="12">
          <Col className="px-xl-2 mx-auto" sm="8" md="6" lg="12">
            <CardTitle style={{ fontSize: '48px' }} tag="h2" className="login-title">
              <p className="mb-1">Merchandise</p>
              Marketplace
            </CardTitle>
            <Form className="auth-login-form mt-2" onSubmit={loginHandler}>
              <div className="mb-1">
                <Label className="form-label" for="login-username">
                  username
                </Label>
                <Input
                  type="username"
                  id="login-username"
                  placeholder="john@example.com"
                  autoFocus
                  value={form.username}
                  onChange={(e) => setForm({ ...form, username: e.target.value })}
                />
              </div>
              <div className="mb-1">
                <div className="d-flex justify-content-between">
                  <Label className="form-label" for="login-password">
                    Password
                  </Label>
                  <Link to="/forgot-password">
                    <small>Forgot Password?</small>
                  </Link>
                </div>
                <InputPasswordToggle
                  className="input-group-merge"
                  id="login-password"
                  value={form.password}
                  onChange={(e) => setForm({ ...form, password: e.target.value })}
                />
              </div>
              <div className="form-check mb-1">
                <Input type="checkbox" id="remember-me" />
                <Label className="form-check-label" for="remember-me">
                  Remember Me
                </Label>
              </div>
              <Button type="submit" color="primary" block>
                Sign in
              </Button>
            </Form>
          </Col>
        </Col>
      </Row>
    </div>
  )
}

export default Login
