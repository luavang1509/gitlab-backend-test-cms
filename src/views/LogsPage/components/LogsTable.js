import React, { useState, useEffect } from 'react'
import { notification } from 'antd'
import DataTable from 'react-data-table-component'
import { ChevronDown } from 'react-feather'
import { Card, Spinner } from 'reactstrap'
import axiosConfig from '../../../configs/axiosConfig'
import { columns } from '../data'
import ReactPaginate from 'react-paginate'

const LogsTable = () => {
  const [pagination, setPagination] = useState({
    currentPage: 1,
    pageSize: 10,
    totalPages: 1,
    totalRows: 0,
  })
  const [logs, setLogs] = useState([])
  const [loading, setLoading] = useState(false)

  const get = async (page) => {
    setLoading(true)
    try {
      await axiosConfig.get('/log', { params: { page } }).then((res) => {
        setLogs(res.data.data)
        setPagination(res.data.meta)
        setLoading(false)
      })
    } catch (error) {
      setLoading(false)
      notification.error({
        message: 'Failed to get logs',
        duration: 2,
      })
    }
  }

  useEffect(() => {
    get()
  }, [])

  // ** Function to handle filter
  const handlePagination = (page) => {
    get(page.selected + 1)
  }

  // ** Custom Pagination
  const CustomPagination = () => {
    return (
      <ReactPaginate
        previousLabel={''}
        nextLabel={''}
        forcePage={pagination?.currentPage - 1}
        onPageChange={(page) => handlePagination(page)}
        pageCount={pagination?.totalPages}
        breakLabel={'...'}
        pageRangeDisplayed={2}
        marginPagesDisplayed={2}
        activeClassName="active"
        pageClassName="page-item"
        breakClassName="page-item"
        nextLinkClassName="page-link"
        pageLinkClassName="page-link"
        breakLinkClassName="page-link"
        previousLinkClassName="page-link"
        nextClassName="page-item next-item"
        previousClassName="page-item prev-item"
        containerClassName={
          'pagination react-paginate separated-pagination pagination-sm justify-content-end pe-1 my-1'
        }
      />
    )
  }

  return (
    <>
      {!loading ? (
        <Card className="react-dataTable">
          <DataTable
            dense
            noHeader
            pagination
            data={logs}
            columns={columns}
            className="react-dataTable"
            sortIcon={<ChevronDown size={10} />}
            paginationComponent={CustomPagination}
            paginationDefaultPage={pagination?.currentPage}
          />
        </Card>
      ) : (
        <div className="f-center flex-column gap-1 p-4">
          <Spinner style={{ color: '#f9d84b', width: '40px', height: '40px' }} />
          Chờ xíu nha...
        </div>
      )}
    </>
  )
}

export default LogsTable
