// ** Third Party Components
import moment from 'moment/moment'
import { Badge } from 'reactstrap'
import { Calendar, CreditCard, File, Info } from 'react-feather'

export const type = {
  Info: { title: 'Info', color: 'light-info' },
  Error: { title: 'Error', color: 'light-danger' },
}

// ** Table Headers
const DeliveryDate = () => (
  <div className="f-center">
    <Calendar />
    <span>Date</span>
  </div>
)
const DownloadedRows = () => (
  <div className="f-center">
    <CreditCard />
    <span>ID</span>
  </div>
)
const FileCSV = () => (
  <div className="f-center">
    <File />
    <span>Message</span>
  </div>
)
const Status = () => (
  <div className="f-center">
    <Info />
    <span>Status</span>
  </div>
)

// ** Table Common Column
export const columns = [
  {
    name: <DeliveryDate />,
    width: '220px',
    cell: (row) => {
      return <div>{moment(row.createdAt).format('DD/MM/YYYY · hh:mm:ss A')}</div>
    },
  },
  {
    name: <DownloadedRows />,
    width: '250px',
    cell: (row) => {
      return <div className="table-id">{row._id || row.id}</div>
    },
  },
  {
    name: <FileCSV />,
    cell: (row) => {
      return <div>{row.message}</div>
    },
  },
  {
    name: <Status />,
    width: '130px',
    cell: (row) => {
      return (
        <Badge color={type[row.type].color} pill>
          {type[row.type].title}
        </Badge>
      )
    },
  },
]
