import LogsTable from './components/LogsTable'
import '@styles/react/libs/tables/react-dataTable-component.scss'

const LogsPage = () => {
  return (
    <>
      <LogsTable />
    </>
  )
}

export default LogsPage
